# Introduction

LibreHealth is an open-source healthcare technology platform, a community formed by individuals from different verticals of the healthcare IT industry. Not every corner of this world is equipped with the best healthcare services and LibreHealth aims at being the bridge connecting the challenged environments to the well equipped ones. 

LibreHealth toolkit can act as a platform between users and health care professionals, where the users/individuals can directly rely on doctors and clinicians practicing healthcare in different corners of the world completely. Our system boasts a diverse community of doctors, academic professionals, and software developers.

LibreHealth aims to unify the world under a single roof built with technology. In this new age of technolgical revolution, the accessibility to basic amenities is still a dream to many communities in under-developed and developing countries. Proper healthcare is one of them. As a community, LibreHealth focuses and works for communities like these, and strives towards achieving this goal.
