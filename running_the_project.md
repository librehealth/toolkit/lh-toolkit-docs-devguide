# Running the Project

In this chapter, we will dicuss the procedure to run lh-installation toolkit on the local system. If the method discussed in the previous chapter is used to open the project, hovering over the selection list on the bottom left of the screen opens the maven projects, which will open by default when the first procedure is followed.

From the tools available above the list, under maven projects, select Toggle 'Skip Tests' Mode which cancels out the test option under all the dropdowns in the Librehealth toolkit. This option is selected mainly to aid the user by not running some optional verification tests each time the module is built. Clicking on install under the lifecycle builds the lifecycle module as shown in the images below.

![](19.JPG)

![](20.JPG)

Install the openmrs.api by selecting the install option from Lifecycle in openmrs.api field. To run the application locally, select openmrs-webapp -> Plugins -> jetty -> jetty:deploy-war. This will help in running the war program and delpoying it on local system using the inbuilt jetty server. The figure below shows how jetty:deploy-war works.

![](21.JPG)

To run it on the local system, jetty:run is run and this option starts a jetty server. By typing localhost:8080/openmrs in the URL of the web browser, the lh-toolkit installation process starts locally. The figures below show how jetty server is launched and the lh-toolkit runs.

![](22.JPG)

![](13.JPG)

