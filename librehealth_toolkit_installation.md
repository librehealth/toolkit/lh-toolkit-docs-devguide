# LibreHealth toolkit installation

The installation procedure first requires that the user has installed MySQL in the system and provided a root password which was set during the installation of MySQL. After opening the URL provided in the previous chapter and going through the next step after selecting the desired language, a screen as shown below appears.

![](14.JPG)

Here, the process to install Simple and Advanced types is described. The Simple type, as shown, creates a demo database and is recommended when running in the local machines. The advanced gives more flexibility over selection of database url, name etc.

# Simple
After selecting the simple option, the application asks for a root password to initiate database creation and update. After entering the root password, it also gives the admin id and password which should be noted down by the user. On entering the root password, a content list giving the details about the database appears.

![](23.JPG)

![](24.JPG)

After verifying the properties of the database and following the next step, the application automatically creates a database and tables. It displays a message showing that the OpenMRS platform is running successfully.

![](25.JPG)

![](26.JPG)

# Advanced

**STEP 1:** 
The advanced option gives the user more flexibility over choosing the database url, name etc. and also additional customization settings during the process of installation. This option is mainly used in production environments. After selecting the advanced option, a screen appears where you can select the database connection and an optional setting to choose the database driver name. The options for connecting to a current database or creating a database can be selected appropriately and providing the 'root' password as described under the simple process.

![](28.JPG)

**STEP 2:**
The toolkit also creates some demo tables and values in the database for reference. If the user is granted a read/write access to the openmrs database other than root, the username and password for that particular user can be entered by specifying the yes/no option. 

![](29.JPG)

**STEP 3:**
The user is prompted whether to upload the modules from the web interface, and this is completely optional to the user. Selecting the default options will be enough to go ahead with the installation.

![](30.JPG)

**STEP 4:**
The admin password which will be given by the application (as shown in the simple section) has to be entered by default. New password for each user can be created later. 

![](31.JPG)

**STEP 5:**
The implementation details are entered, and as the descriptions are shown beside the titles, it is important to name them uniquely with corresponding descriptions in case of many similar installations of openmrs.

![](32.JPG)

After completing all the 5 steps of advanced installation, a screen appears (like the confirmation screen in simple installation), where all the details entered by the user and the installation directory will be specified. In the next step, the database creation and installation will happen as described in the Simple Installation procedure.