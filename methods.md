# LibreHealth toolkit

The lh-toolkit is a software API and interface which forms the basis to create health record systems. This forms the basis of LibreHealth where users can create their own customized medical record system with minimal or no programming knowledge. Since it is accessible to users from different departments like researchers, clinicians, planners, epidemiologists and patients, the lh-toolkit can be considered as the core part of LibreHealth project.

# Cloning

The lh-toolkit is one of the projects under the LibreHealth group in GitLab. It contains some of the most important softwares and interfaces required to run efficiently and to co-ordinate with other projects of LibreHealth like librehealth-radiology and open-decision-framework. The table below can be referred to understand the repositories present in the lh-toolkit.

|                  |              |  
|:-----------------|:-------------|
|api/              |java and resource files for building the java api jar file. |
|puppet            |Puppet scripts for local development and deployment.|
|release-test      |Cucumber/selenium integration tests. Run daily against a running web app.|
|test              |Maven project to share tools that are used for testing.|
|tools             |Meta code used during compiling and testing. Does not go into any released binary (like doclets).|
|web/              |java and resource files that are used in the web application.|
|webapp/           |jsp files used in building the war file.|
|.gitlab-ci.yaml   |Used to configure Gitlab CI. Each branch can have its own configuration.|
|license-header.txt|Used by license-maven-plugin to check the license on all source code headers.|
|pom.xml           |The main maven file used to build and package OpenMRS.|

As we can observe, this project should be opened under maven projects, else there is a possibility that all the models might not open correctly. Once the project is forked into the user's profile successfully, it is cloned to the local system easily using the git clone command. Many IDE's are available on the web but IntelliJ IDEA is preferred in this case considering its flexibility in handling complex projects like this.

## Using Githab
Please use our [Gitlab repository](https://gitlab.com/librehealth) as it contains the up-to-date version of the project.

## Using Gitlab
The project can be cloned into any of the desired folders in the local system using IntelliJ IDEA itself. After opening the IDE for the first time, from the menu as shown below, select the VCS. Check out from version control and then the Git option from the dropdown where the application asks for the URL of a project repository.
From the image shown below, you can observe that a local directory is also needed to be defined to save the project.

![](27.JPG)

By proceeding with the next steps, the project gets loaded into the IDE and it automatically shows the available folders and also Maven Projects which are useful in building and deployment. The maven projects on the right side of the screen can be opened by selecting maven projects option by hovering over the icon on the bottom left of the screen. A reminder that should be considered while installation is that the project should be selected as a maven project and there should be a relevant SDK installed on the system. In this case, JDK 1.8 would suffice and the IDE will detect everything by default once an updated version is available.
![](17.JPG)
 
Another way to open the project is by cloning it into the local system using git commands and following the process shown in the next chapter. This process is a lengthy one and requires the user to manually select options to build the project. This above-mentioned procedure is technically more simple and can save time. For users who require more understanding into the process of extraction, the procedure is explained in the next chapter.